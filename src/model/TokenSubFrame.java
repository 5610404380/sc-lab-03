package model;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class TokenSubFrame extends JFrame {
   public TokenSubFrame() {
	   createFrame();
	   
   }
   public void createFrame() {
	   calButton = new JButton("calculate");
	   showResults = new JTextArea("Enter word here");
	   textfield = new JTextField("Enter ngram here");
	   radio1 = new JRadioButton("word");
	   radio2 = new JRadioButton("ngram word",true);
	   ButtonGroup rdGroup = new ButtonGroup();
	   rdGroup.add(radio2);
	   rdGroup.add(radio1);
	   radio2.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	TokenSub t  = new TokenSub();
	        	extendResult(t.str2);

	        }
	    });
	   radio1.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	TokenSub t  = new TokenSub();
	        	extendResult(t.str1);
	        	
	        }
	    });
	   
	   JPanel panel1 = new JPanel();
	   JPanel panel2 = new JPanel();
	   JPanel panel3 = new JPanel(new BorderLayout());
	   JPanel rdpanel = new JPanel(new GridLayout(1,2));
	   panel1.setLayout(new GridLayout(1,2));
	   panel3.setLayout(new GridLayout(2,1));
	   panel1.add(panel2);
	   panel1.add(panel3);
	   panel2.add(showResults); 
	   panel3.add(textfield);
	   panel3.add(rdpanel);
	   rdpanel.add(radio2);
	   rdpanel.add(radio1);
	   
	   add(calButton, BorderLayout.SOUTH); 
	   
	   add(panel1,BorderLayout.CENTER);
	   
   }
   public void setResult(String str) {
       this.str = str;
	   showResults.setText(str);	   
	  
   }
   
   public String getResult() {
	   return showResults.getText();
   }	   
   public String getField() {
	   return textfield.getText();
	  
   }
   public void extendResult(String str) {
	   this.str = this.str+"\n"+str;
	   showResults.setText(this.str);
   }
   public void setListener(ActionListener list) {
	   calButton.addActionListener(list);
   }

   
   private JPanel panel1;
   private JPanel panel2;
   private JPanel panel3;
   private JPanel rdpanel;
   private JRadioButton radio1,radio2;
   private JTextField textfield;
   private JTextArea showResults;
   private JButton calButton;
   private String str;
   public String str2 = "a";
   public String str3 = "b";
}

